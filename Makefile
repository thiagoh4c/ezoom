install:
	@echo "Creating docker containers "
	@echo ""
	cd containers && docker-compose up -d
	@echo ""
	@echo "Created"
	@echo ""
	@echo ""
	@echo "Creating app project"
	cd ezoom-app && npm install
	npm i -D -E @ionic/lab


pull:
	@echo ""
	@echo "Downloading project from git"
	git pull origin master
	@echo "Complete"

start:
	docker-compose up -d
	docker exec thiago_apachephp service apache2 start
	cd ezoom-app && ionic serve -lc &
	cd html && php spark serve 

php:
	docker-compose up -d
	docker exec thiago_apachephp service apache2 start
	cd html && php spark serve 

restart: 
	docker-compose stop
	docker-compose up -d
	docker exec thiago_apachephp service apache2 start
